clc
clear all
close all
load('groundtruth.mat')
load('filter_norm_expression.mat')

A = A([1,2,11,12,13,14,15,16],[1,2,11,12,13,14,15,16]);
expression = expression(:,[1,2,11,12,13,14,15,16]);

save('groundtruth.mat', 'A')
save filter_norm_expression.mat expression;

