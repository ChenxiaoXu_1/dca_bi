% This file deals with generating the data using linear model. 
clear all
close all
% Load the regulatory pairs from the golden standard. 
n = 39;

load pairs_golden_standard;
pairs = pairs_golden_standard;
load self_golden_standard;
self = self_golden_standard;

% Generating the regulatory coefficients
% Not needed anymore due to 
% pairs(:,3)= -1 + 2.*rand(regnum, 1)

% Generating the initial few time points
T=1003;
expression = zeros(n, T);

% Generating the expression values
% First 1:3 time points are random data
% Maximum model order considered here is 3

%Please do not change this random generator
rng(200) 
expression(1:n,1)=normrnd(0,1,n,1);
expression(1:n,2)=normrnd(0,1,n,1);
expression(1:n,3)=normrnd(0,1,n,1);

% Calculate autoregression coefficient
% 36 time points with 3 cycles are considered 
a = exp((1/6)*pi*1.0i);
b = exp(-(1/6)*pi*1.0i);
c = a+b;
d = a*b;

for i=4:T
    for j=1:n
        % Initialization put in the noise value
        expression(j,i) = normrnd(0, 1);

        % Calculating the expression value for "j"
        % Find the causes of j
        array = find(pairs(:,2)==j);

        % Here actually only the top layer does not have a regulator
        % They are dealt with using auto regulation
        if size(array, 1)==0
            index=find(self(:,2)==j);
            expression(j,i) = expression(j,i) +  tanh((c*self(index, 4)*expression(j,i-1) * d * self(index,4) * self(index,4) * expression(j,i-2))^2);            
        else
            % The regulated part did not use auto-regulation
            % Using the random coefficient to generate the network
            for k=1:size(array,1)
                expression(j,i) =  expression(j,i) + log((pairs(array(k),4)*expression(pairs(array(k),1), i-pairs(array(k), 3)))^2);
            end
        end
    end
end

%no noise
expression0 = expression(:,4:end);
%noise level 1 0.8-1.2
rng(1)
expression1 = expression0 .* (0.8+0.4*rand(n,T-3));
%noise level 2 0.5-1.5
rng(2)
expression2 = expression0 .* (0.5+1.0*rand(n,T-3));
%noise level 3 0.2-1.8
rng(3)
expression3 = expression0 .* (0.2+1.6*rand(n,T-3));

[expression0] = standalization(expression0);
[expression1] = standalization(expression1);
[expression2] = standalization(expression2);
[expression3] = standalization(expression3);

figure(1)
plot(expression0)
figure(2)
plot(expression1)
figure(3)
plot(expression2)
figure(4)
plot(expression3)

expression = expression0;
save filter_norm_expression0.mat expression;
expression = expression1;
save filter_norm_expression1.mat expression;
expression = expression2;
save filter_norm_expression2.mat expression;
expression = expression3;
save filter_norm_expression3.mat expression;
