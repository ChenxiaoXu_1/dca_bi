% Generate the simulation regulatory network using MATLAB. 
% Three layer structure. 
% Check the README in this folder. 

% Initiating the Adjancency Matrix
% 1110*1110 non-symmetric matrix 
% A(i, j) represents the causality from i->j. 

clc
clear all
close all

rng(1234)

n=39;
A=zeros(n, n);

topLayer = 1:3;
middleLayer = 4:12;
bottomLayer = 13:39;

pairs_filename='pairs_golden_standard';
pairs_wid=fopen(pairs_filename, 'w');

self_filename='self_golden_standard';
self_wid=fopen(self_filename, 'w');

% Generating the top to middle edges
for i=topLayer
    A(i, (i*3+1):(i*3+3))=1;
end

% Generating the middle to bottom edges
for i=middleLayer
    A(i, (i*3+1):(i*3+3))=1;
end

% Randomly perturb the regulatory network
% The percentage is 60 in 60*180 possible edges. 
for i=1:3
    temp1 = randi(3);
    temp2 = randi(9)+3;
    A(temp1, temp2) = 1;
end

% The percentage is 180 in 180*180 possible edges.  
for i=1:12
    temp1 = randi(9)+3;
    temp2 = randi(9)+3;
    A(temp1, temp2) = 1;
end

% The percentage is 540 in 540*180 possible edges
for i=1:27
    temp1 = randi(9)+3;
    temp2 = randi(27)+12;
    A(temp1, temp2) = 1;
end


for i = size(A,1)
    A(i,i) = 0;
end


% Print out self regularization including decay factor as a key parameter. 
array=sum(A);
for i=1:length(array)
    if array(i)==0
        % Half of the master nodes are not activated
        fprintf(self_wid, '%d\t%d\t%d\t%f\n', i, i, 1, 0.95+0.05*rand());
    end
end

% Print out the Regulatory network
% The regulatory coefficient are generated from -1 to 1 by
% a uniform distribution
% The model order p is controlled by randi from 1 to 3
for i=1:size(A,1)
    for j=1:size(A,2)
        if i~=j && A(i, j)~=0
            fprintf(pairs_wid, '%d\t%d\t%d\t%f\n', i, j, randi(3), -1+2*rand()); 
        end
    end
end


imagesc(A)

save('groundtruth.mat', 'A')

fclose(pairs_wid);
fclose(self_wid);
%exit;
